<?php
Class Database {

    private $_password = "linuxguy";
    private $_user = "root";
    private $_dsn = "mysql:dbname=wokondb;host=localhost";
    private $_dbh;

    public function __construct()
    {
        $this->_dbh = new PDO($this->_dsn, $this->_user, $this->_password);
    }

    public function getCon()
    {
        return $this->_dbh;
    }

    public function insertDataIntoBiodata($firstName, $lastName, $gender, $day, $month, $year, $userId, $urlName, $position, $date,
    $cityId, $stateId, $countryId, $avatar, $bio, $username)
  	{
  		$st =$this->_dbh->prepare("INSERT INTO UserBiodata
  			(FirstName, LastName, Gender,  DayOfBirth, MonthOfBirth, YearOfBirth,  UserId, Urlname, RegistrationStep, DateJoined, CityId,
        StateId, countryId, Avatar, BioInfo, Phone)
         VALUES
  		(:firstname, :lastname, :gender, :day,  :month,  :year,:userId, :urlname, :position, :dateJoined, :cityId, :stateId, :countryId,
      :avatar, :BioInfo, :Phone) ");

  		$st->bindValue(":userId", $userId, PDO::PARAM_STR);
  		$st->bindValue(":firstname", $firstName, PDO::PARAM_STR);
  		$st->bindValue(":lastname", $lastName, PDO::PARAM_STR);
  		$st->bindValue(":gender", $gender, PDO::PARAM_STR);
  		$st->bindValue(":day", $day, PDO::PARAM_STR);
  		$st->bindValue(":month", $month, PDO::PARAM_STR);
  		$st->bindValue(":year", $year, PDO::PARAM_STR);
  		$st->bindValue(":urlname", $urlName, PDO::PARAM_STR);
  		$st->bindValue(":position", $position, PDO::PARAM_INT);
  		$st->bindValue(":dateJoined", $date, PDO::PARAM_STR);
      $st->bindValue(":cityId", $cityId, PDO::PARAM_STR);
      $st->bindValue(":stateId", $stateId, PDO::PARAM_STR);
      $st->bindValue(":countryId", $countryId, PDO::PARAM_STR);
      $st->bindValue(":avatar", $avatar, PDO::PARAM_STR);
      $st->bindValue(":BioInfo", $bio, PDO::PARAM_STR);
      $st->bindValue(":Phone", $username, PDO::PARAM_STR);
  		if($st->execute()){ return true; }else{ return false; }
  	}

    public function insertIntoAuth($userId, $password, $username, $usernameVerificationStatus)
    {
      $st =$this->_dbh->prepare("INSERT INTO UserAuthentication
  			(UserId, Password, Username, UsernameVerificationStatus)
         VALUES
  		(:UserId, :Password, :Username, :UsernameVerificationStatus) ");

  		$st->bindValue(":UserId", $userId, PDO::PARAM_STR);
  		$st->bindValue(":Password", $password, PDO::PARAM_STR);
  		$st->bindValue(":Username", $username, PDO::PARAM_STR);
  		$st->bindValue(":UsernameVerificationStatus", $usernameVerificationStatus, PDO::PARAM_INT);
      if($st->execute()){ return true; }else{ return false; }
    }


    public function sendPalRequest($userId1, $userId2, $friendStatus, $notified, $time, $seen)//sends pal request by inserting into palrequest table
  	{

  	$st =$this->_dbh->prepare("Insert into PalRequest (SenderId, RecieverId, FriendStatus, Notified, Time, Seen) values (:userId1, :userId2,:friendStatus, :notified, :time, :seen)");
  		$st->bindValue(':userId1', $userId1, PDO::PARAM_STR);
  		$st->bindValue(':userId2', $userId2, PDO::PARAM_STR);
  		$st->bindValue(':friendStatus', $friendStatus, PDO::PARAM_INT);
  		$st->bindValue(':notified', $notified, PDO::PARAM_INT);
  		$st->bindValue(':time', $time, PDO::PARAM_STR);
  		$st->bindValue(':seen', $seen, PDO::PARAM_INT);
  		return ($st->execute()) ? true : false;
  	}


    public function acceptPalRequest($userId1, $userId2, $addedDate,  $friendStatus)
  	{

  		$st =$this->_dbh->prepare("Insert into UserPals (UserId, PalUserId, AddedDate, FriendStatus) values (:userId1, :userId2,:addedDate, :friendStatus)");

  		$st->bindValue(':userId1', $userId1, PDO::PARAM_STR);
  		$st->bindValue(':userId2', $userId2, PDO::PARAM_STR);
  		$st->bindValue(':addedDate', $addedDate, PDO::PARAM_STR);
  		$st->bindValue(':friendStatus', $friendStatus, PDO::PARAM_INT);
  		return ($st->execute()) ? true : false;
  	}


    public function logMoment($momentId, $userId , $content, $time, $privacy, $OriginalMomentId, $type)
    {
      	$st =$this->_dbh->prepare("insert into Moments(MomentId, UserId, Content, Time, Privacy, OriginalMomentId, Type)
      values (:MomentId, :UserId,:Content, :Time, :Privacy, :OriginalMomentId, :type)");

       $st->bindValue(':MomentId', $momentId, PDO::PARAM_STR);
       $st->bindValue(':UserId', $userId, PDO::PARAM_STR);
       $st->bindValue(':Content', $content, PDO::PARAM_STR);
       $st->bindValue(':Time', $time, PDO::PARAM_STR);
       $st->bindValue(':Privacy', $privacy, PDO::PARAM_INT);
       $st->bindValue(':OriginalMomentId', $OriginalMomentId, PDO::PARAM_STR);
       $st->bindValue(':type', $type, PDO::PARAM_INT);
       return ($st->execute()) ? true : false;
    }



  }
